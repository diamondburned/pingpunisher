package main

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
	scribble "github.com/nanobox-io/golang-scribble"
)

var db, _ = scribble.New("./", nil)

func initialize() map[string]int {
	var database = make(map[string]int)

	var err = db.Read("./", "database", &database)
	if err != nil {
		println("Database invalid. Resetting.")
		_ := db.Write("./", "database", database)
		os.Exit(1)
	}

	return database
}

var database = initialize()

func main() {
	token := os.Args[1]
	if len(token) == 0 || token == "" {
		fmt.Println("Missing token for arg 0")
		os.Exit(1)
	}

	discord, err := discordgo.New("Bot " + token)
	if err != nil {
		panic(err)
	}

	discord.AddHandler(messageCreate)
	discord.Open()

	println("Bot is ready.")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	discord.Close()
}

func messageCreate(session *discordgo.Session, message *discordgo.MessageCreate) {
	if len(message.Mentions) >= 8 {
		channel, err := session.Channel(message.ChannelID)
		if err != nil {
			fmt.Println("Failed fetching Channel ID " + message.ChannelID + ", reason: \n" + err)
			return
		}

		var database = make(map[string]int)

		var err = db.Read("./", "database", &database)
		if err != nil {
			println("Database invalid.")
		}

		warn := fmt.Sprintf("User %s#%s (ID %s, verified %v, bot %v) kicked/banned for mass-pinging (%v pinged)",
			message.Author.Username,
			message.Author.Discriminator,
			message.Author.ID,
			message.Author.Verified,
			message.Author.Bot,
			len(message.Mentions))

		if database[message.Author.ID] != nil && database[message.Author.ID] >= 1 {
			if err := session.GuildBanCreateWithReason(channel.GuildID, message.Author.ID, warn); err != nil {
				fmt.Println("Failed to delete user " + message.Author.ID + ", reason: \n" + err)
				return
			}
		} else {
			if err := session.GuildMemberDeleteWithReason(channel.GuildID, message.Author.ID, warn); err != nil {
				fmt.Println("Failed to delete user " + message.Author.ID + ", reason: \n" + err)
				return
			}
		}

		if database[message.Author.ID] == nil {
			database[message.Author.ID] = 0
		}

		database[message.Author.ID] += 1

		if err := db.Write("./", "database", database); err != nil {
			fmt.Println("Failed to write to database, reason: \n" + err)
			return
		}
	}
}
